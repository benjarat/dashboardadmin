import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableattendanceComponent } from './tableattendance.component';

describe('TableattendanceComponent', () => {
  let component: TableattendanceComponent;
  let fixture: ComponentFixture<TableattendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableattendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableattendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
