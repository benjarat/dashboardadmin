import { Component, OnInit , HostListener, ViewChild, Injectable} from '@angular/core';
import { CallapiService } from '../callapi.service'
import { from } from 'rxjs';

import {NgbDate, NgbCalendar, NgbDateParserFormatter,
  NgbDateStruct, NgbDateAdapter} from '@ng-bootstrap/ng-bootstrap';

  


@Component({
  selector: 'app-tableattendance',
  templateUrl: './tableattendance.component.html',
  styleUrls: ['./tableattendance.component.scss']
})
export class TableattendanceComponent implements OnInit {

  
  p: number = 1;
  hoveredDate: NgbDate;
  Date: NgbDate;
  fromDate: NgbDate;
  toDate: NgbDate;
  model2 ;
  data = []; 
  constructor(private api : CallapiService,private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private dateAdapter: NgbDateAdapter<string>){
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
  }

  searchText: string;

  
  users: any[] = [
    { name: 'Kristy', gender: 'female' },
    { name: 'Nick', gender: 'male'  },
    { name: 'Ariana', gender: 'female' },
    { name: 'Joe', gender: 'male' },
    { name: 'Albert', gender: 'male' },
    { name: 'Nick', gender: 'male'  },
    { name: 'Ariana', gender: 'female' },
    { name: 'Joe', gender: 'male' },
    { name: 'Albert', gender: 'male' },
    { name: 'Nick', gender: 'male'  },
    { name: 'Ariana', gender: 'female' },
    { name: 'Joe', gender: 'male' },
    { name: 'Albert', gender: 'male' },
    { name: 'Nick', gender: 'male'  },
    { name: 'Ariana', gender: 'female' },
    { name: 'Joe', gender: 'male' },
    { name: 'Albert', gender: 'male' },
    { name: 'Nick', gender: 'male'  },
    { name: 'Ariana', gender: 'female' },
    { name: 'Joe', gender: 'male' },
    { name: 'Albert', gender: 'male' },
    { name: 'Nick', gender: 'male'  },
    { name: 'Ariana', gender: 'female' },
    { name: 'Joe', gender: 'male' },
    { name: 'Albert', gender: 'male' }
  ];


 
  ngOnInit(){
    this.api.getTable(this.formatter.format(this.calendar.getToday())).subscribe((table)=>{
      this.data = table;
    })
  }

   alert() {
    this.api.getTable(this.formatter.format(this.model2)).subscribe((table)=>{
      this.data = table;
    })
     
  }

   onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

}
  
 


