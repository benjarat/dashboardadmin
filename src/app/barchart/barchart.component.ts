import { Component, OnInit } from '@angular/core';
import { Label, Color } from 'ng2-charts';
import { CallapiService } from '../callapi.service'

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.scss']
})
export class BarchartComponent implements OnInit {
    public dataIn = [];
    public dataOut = [];
  constructor(private api : CallapiService) { }
  
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = ['06:00', '08:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [

    {data: this.dataIn, label: 'Entry'},
    {data: this.dataOut, label: 'Exit'}
  ];
  public barChartColors: Color[] = [
    { backgroundColor: '#3cd6d6' },
    { backgroundColor: '#e6397b' }
  ]
  ngOnInit() {
    setInterval(() => {
      this.dataIn = [];
      this.dataOut = [];
      this.api.getPeriod().subscribe((time)=>{
        time.forEach(element => {
       
              this.dataIn.push(element.in)
              this.dataOut.push(element.out)
          

        });
        this.barChartData = [
          {data: this.dataIn, label: 'Entry'},
          {data: this.dataOut, label: 'Exit'}
        ];
        console.log(this.dataIn)
  })

     }, 10000);

  }

}
