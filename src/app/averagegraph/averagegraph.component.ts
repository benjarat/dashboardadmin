import { Component, OnInit } from '@angular/core';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { CallapiService } from '../callapi.service';

@Component({
  selector: 'app-averagegraph',
  templateUrl: './averagegraph.component.html',
  styleUrls: ['./averagegraph.component.scss']
})
export class AveragegraphComponent implements OnInit {

  public happy = [];
  public unhappy = [];
  public nuetural = [];
constructor(private api : CallapiService) { }

public barChartOptions = {
  scaleShowVerticalLines: false,
  responsive: true
};
public barChartLabels = ['06:00', '08:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00'];
public barChartType = 'bar';
public barChartLegend = true;
public barChartData = [
  
  {data: [0,0,0,0,0,0,0,0], label: 'Happy'},
  {data: [0,0,0,0,0,0,0,0], label: 'Neutral'},
  {data: [0,0,0,0,0,0,0,0], label: 'Unhappy'}
];
public barChartColors: Color[] = [
  { backgroundColor: '#2993d9' },
  { backgroundColor: '#adadaa' },
  { backgroundColor: '#d93229' },
]
  ngOnInit() {
    setInterval(() => {
    
      
      this.api.getAverage().subscribe((emotion)=>{
        this.happy = [];
        this.unhappy = [];
        this.nuetural = [];
        console.log(emotion)
             emotion.forEach((element)=>{
              this.happy.push(element.happy);
              this.unhappy.push(element.unhappy);
              this.nuetural.push(element.netural);
             })
        
      });
      this.barChartData = [
        {data: this.happy, label: 'Happy'},
        {data: this.nuetural, label: 'Neutral'},
        {data: this.unhappy, label: 'Unhappy'}
      ];
  },10000);
  }


}
