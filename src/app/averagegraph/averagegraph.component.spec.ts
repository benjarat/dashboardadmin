import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AveragegraphComponent } from './averagegraph.component';

describe('AveragegraphComponent', () => {
  let component: AveragegraphComponent;
  let fixture: ComponentFixture<AveragegraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AveragegraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AveragegraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
