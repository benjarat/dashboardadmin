import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllpiechartComponent } from './allpiechart.component';

describe('AllpiechartComponent', () => {
  let component: AllpiechartComponent;
  let fixture: ComponentFixture<AllpiechartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllpiechartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllpiechartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
