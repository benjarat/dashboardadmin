import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AveragegraphComponent } from './averagegraph/averagegraph.component';
import { BarchartComponent } from './barchart/barchart.component';
import { PiechartComponent } from './piechart/piechart.component';
import { CountemotionComponent } from './countemotion/countemotion.component';
import { TableattendanceComponent } from './tableattendance/tableattendance.component';
import { ChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';
import { TableFilterPipe } from '../app/tableattendance/table-filter.pipe';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
import { AlldashboardComponent } from './alldashboard/alldashboard.component';
import { AllpiechartComponent } from './allpiechart/allpiechart.component';
import { AllcountemotionComponent } from './allcountemotion/allcountemotion.component';
import { AllemobarComponent } from './allemobar/allemobar.component';

@NgModule({
  declarations: [
    AppComponent,
    AveragegraphComponent,
    BarchartComponent,
    PiechartComponent,
    CountemotionComponent,
    TableattendanceComponent,
    TableFilterPipe,
    DashboardComponent,
    LoginComponent,
    AlldashboardComponent,
    AllpiechartComponent,
    AllcountemotionComponent,
    AllemobarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
