import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CallapiService {

  constructor(private httpClient:HttpClient) { }
  private baseUrl = 'http://20.188.110.129:3000/';

  getPeriod() {
    return this.httpClient.get<any[]>(`${this.baseUrl}` + 'getgrapghperiod');
  }

  getEmotion(){
    return this.httpClient.get<any[]>(`${this.baseUrl}` + 'countemo');
  }

  getOntime(){
    return this.httpClient.get<any[]>(`${this.baseUrl}` + 'getcountlate');
  }
  
  getAverage(){
    return this.httpClient.get<any[]>(`${this.baseUrl}` + 'getemograph');
  }

  getTable(date){
    return this.httpClient.get<any[]>(`${this.baseUrl}` + 'getcheckattendancetable/' + `${date}`);
  }

  getOntimeWeek(){
    return this.httpClient.get<any[]>(`${this.baseUrl}` + 'getcountlateweek');
  }

  getEmotionWeek(){
    return this.httpClient.get<any[]>(`${this.baseUrl}` + 'countemoperiodweek');
  }

  getCountEmoWeek(){
    return this.httpClient.get<any[]>(`${this.baseUrl}` + 'countemoweek');
  }

  
  // getEmotionWeek(){
  //   return this.httpClient.get<any[]>(`${this.baseUrl}` + 'countemoperiodweek');
  // }
  
}

