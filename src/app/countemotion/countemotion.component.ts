import { Component, OnInit } from '@angular/core';
import { CallapiService } from '../callapi.service'

@Component({
  selector: 'app-countemotion',
  templateUrl: './countemotion.component.html',
  styleUrls: ['./countemotion.component.scss']
})
export class CountemotionComponent implements OnInit {

  constructor(private api : CallapiService) { }
  public nuteral = 0;
  public happy = 0;
  public surprise = 0;
  public sadness = 0;
  public angry = 0;
  public disgust = 0;
  public contempt = 0;
  public fear = 0;
  ngOnInit() {
    setInterval(() => {
      this.api.getEmotion().subscribe((emotion)=>{
        this.nuteral = emotion['neutral'];
        this.happy = emotion['happiness'];
        this.surprise = emotion['surprise'];
        this.sadness = emotion['sadness'];
        this.angry = emotion['anger'];
        this.disgust = emotion['disgust'];
        this.contempt = emotion['contempt'];
        this.fear = emotion['fear'];
        });
  },10000);
} 

}
