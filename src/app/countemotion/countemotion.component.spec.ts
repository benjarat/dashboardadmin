import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountemotionComponent } from './countemotion.component';

describe('CountemotionComponent', () => {
  let component: CountemotionComponent;
  let fixture: ComponentFixture<CountemotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountemotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountemotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
