import { Component, OnInit } from '@angular/core';
import { CallapiService } from '../callapi.service'

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.scss']
})
export class PiechartComponent implements OnInit {
  public ontime = 10;
  public absense = 10;
  public late = 10;
  constructor(private api : CallapiService) { }
  public pieChartColors:Array<any> = [{
    backgroundColor: ['#2eb82e','#b3b3b3' ,'#EE8522'], //'#2eb82e', '#b3b3b3', '#ff8000'
    borderColor: ['#26a326','#949494' ,'#e07000']
  }];
  public pieChartLabels = ['On time', 'Late', 'Absence'];
  public pieChartData = [Number(this.ontime), Number(this.late), Number(this.absense)];
  public pieChartType = 'pie';
  ngOnInit() {
    setInterval(() => {
      this.api.getOntime().subscribe((user)=>{
        this.pieChartData =  [user['ontime'],  user['late'], user['absence']];
        });
  },10000);
  }

}
