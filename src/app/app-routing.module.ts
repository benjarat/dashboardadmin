import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BarchartComponent } from './barchart/barchart.component';
import { PiechartComponent } from './piechart/piechart.component';
import { AveragegraphComponent } from './averagegraph/averagegraph.component';
import { CountemotionComponent } from './countemotion/countemotion.component';
import { TableattendanceComponent } from './tableattendance/tableattendance.component';
import { DashboardComponent } from './dashboard/dashboard.component'
import { AlldashboardComponent } from './alldashboard/alldashboard.component'

const routes: Routes = [
  {path: 'bar', component: BarchartComponent},
  {path: 'pie', component: PiechartComponent},
  {path: 'average', component: AveragegraphComponent},
  {path: 'count', component: CountemotionComponent},
  {path: 'table', component: TableattendanceComponent },
  {path: 'overall', component: AlldashboardComponent},
  {path: '**', component: DashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
