import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllcountemotionComponent } from './allcountemotion.component';

describe('AllcountemotionComponent', () => {
  let component: AllcountemotionComponent;
  let fixture: ComponentFixture<AllcountemotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllcountemotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllcountemotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
