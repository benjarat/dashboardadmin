import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllemobarComponent } from './allemobar.component';

describe('AllemobarComponent', () => {
  let component: AllemobarComponent;
  let fixture: ComponentFixture<AllemobarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllemobarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllemobarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
