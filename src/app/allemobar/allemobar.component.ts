import { Component, OnInit ,ViewChild} from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { CallapiService } from '../callapi.service';

@Component({
  selector: 'app-allemobar',
  templateUrl: './allemobar.component.html',
  styleUrls: ['./allemobar.component.scss']
})
export class AllemobarComponent implements OnInit {

  public happy = [];
  public unhappy = [];
  public nuetural = [];
  public label = [];
constructor(private api : CallapiService) { }

public barChartOptions = {
  scaleShowVerticalLines: false,
  responsive: true
};
public lineChartLabels = ['06:00', '08:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00'];
public barChartType = 'bar';
public barChartLegend = true;
public lineChartData = [
  
  {data: [0,0,0,0,0,0,0,0], label: 'Happy'},
  {data: [0,0,0,0,0,0,0,0], label: 'Nuetral'},
  {data: [0,0,0,0,0,0,0,0], label: 'Unhappy'}
];
public lineChartOptions: (ChartOptions & { annotation: any }) = {
  responsive: true,
  scales: {
    // We use this empty structure as a placeholder for dynamic theming.
    xAxes: [{}],
    yAxes: [
      {
        id: 'y-axis-0',
        position: 'left',
      }
      
    ]
  },
  annotation: {
    annotations: [
      {
        type: 'line',
        mode: 'vertical',
        scaleID: 'x-axis-0',
        value: 'March',
        borderColor: 'orange',
        borderWidth: 2,
        label: {
          enabled: true,
          fontColor: 'orange',
          content: 'LineAnno'
        }
      },
    ],
  },
};
public lineChartColors: Color[] = [
  { // blue
    backgroundColor: 'rgba(41, 147, 217, 0.5)',
    borderColor: 'rgba(41, 147, 217,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: 'rgba(41, 147, 217,1)',
    pointHoverBackgroundColor: 'rgba(41, 147, 217, 0.5)',
    pointHoverBorderColor: 'rgba(41, 147, 217,0.8)',
  },
  { // grey
    backgroundColor: 'rgba(173, 173, 170,0.5)',
    borderColor: 'rgba(173, 173, 170,1)',
    pointBackgroundColor: 'rgba(173, 173, 170,0.5)',
    pointBorderColor: 'rgba(173, 173, 170,1)',
    pointHoverBackgroundColor: 'rgba(173, 173, 170,0.5)',
    pointHoverBorderColor: 'rgba(173, 173, 170,0.8)',
  },
  { // red
    backgroundColor: 'rgba(217, 50, 41,0.5)',
    borderColor: 'rgba(217, 50, 41,1)',
    pointBackgroundColor: 'rgba(217, 50, 41,1)',
    pointBorderColor: 'rgba(217, 50, 41,1)',
    pointHoverBackgroundColor: 'rgba(217, 50, 41,0.5)',
    pointHoverBorderColor: 'rgba(217, 50, 41,0.8)'
  }
];
public lineChartLegend = true;
public lineChartType = 'line';
public lineChartPlugins = [pluginAnnotations];

@ViewChild(BaseChartDirective) chart: BaseChartDirective;

  ngOnInit() {
    setInterval(() => {
    
      
      this.api.getEmotionWeek().subscribe((emotion)=>{
        this.happy = [];
        this.unhappy = [];
        this.nuetural = [];
        this.label = [];
        console.log(emotion)
             emotion.forEach((element)=>{
              this.happy.push(element.happy);
              this.unhappy.push(element.unhappy);
              this.nuetural.push(element.netural);
              this.label.push(element.day);
             })
        
      });
      this.lineChartData = [
        {data: this.happy, label: 'Happy'},
        {data: this.nuetural, label: 'Neutral'},
        {data: this.unhappy, label: 'Unhappy'}
      ];

      this.lineChartLabels = this.label;
  },10000);
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public hideOne() {
    const isHidden = this.chart.isDatasetHidden(1);
    this.chart.hideDataset(1, !isHidden);
  }

 

  public changeColor() {
    this.lineChartColors[2].borderColor = 'green';
    this.lineChartColors[2].backgroundColor = `rgba(0, 255, 0, 0.3)`;
  }


}
